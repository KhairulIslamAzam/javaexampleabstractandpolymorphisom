package abstractclasspolymorphysom;

public class CommisionEmployee extends Employee {
    private double commissionsRate;
    private double gross;

    public CommisionEmployee(String firstName, String lastName, String mobile, double commisionRate, double gross) {
        super(firstName, lastName, mobile);

        if (gross <= 0) {
            throw new IllegalArgumentException("Gross must be greater then Zero");

        }
        if (commisionRate <= 0 || commisionRate >= 1) {
            //this.commisionRate = 0;
            throw new IllegalArgumentException("Commision must be between 0 -1");
        }

        this.commissionsRate = commisionRate;
        this.gross = gross;
    }

    public double getCommissionsRate() {
        return commissionsRate;
    }

    public double getGross() {
        return gross;
    }

    @Override
    public double earnings() {
        return gross + calculateCommissionRate();
    }

    public double calculateCommissionRate() {
        return commissionsRate * gross;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "commisionRate = " + commissionsRate + "\n" +
                "gross = " + gross;
    }
}
