package abstractclasspolymorphysom;

public abstract class Employee {
    private String firstName;
    private String lastName;
    private String mobile;

    public Employee(String firstName, String lastName, String mobile) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMobile() {
        return mobile;
    }

    @Override
    public String toString() {
        return "Name = " + firstName + " " + lastName + "\n" +
                "mobile = " + mobile;
    }

    public abstract double earnings();
}
