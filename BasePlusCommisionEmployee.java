package abstractclasspolymorphysom;

public class BasePlusCommisionEmployee extends CommisionEmployee {
    private double baseSalary;

    public BasePlusCommisionEmployee(String firstName, String lastName, String mobile, double commisionRate, double gross, double baseSalary) {
        super(firstName, lastName, mobile, commisionRate, gross);

        if (baseSalary <= 0) {
            throw new IllegalArgumentException("Basic Salary must Be greater then Zero");
        }

        this.baseSalary = baseSalary;
    }

    public double getBaseSalary() {
        return baseSalary;
    }

    @Override
    public double earnings() {
        return baseSalary + super.earnings();
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "Base Salary = " + baseSalary;
    }
}
