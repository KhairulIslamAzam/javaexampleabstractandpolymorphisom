package abstractclasspolymorphysom;

public class HourlyEmployee extends Employee {
    private double wage;
    private int hour;

    public HourlyEmployee(String firstName, String lastName, String mobile, double wage, int hour) {
        super(firstName, lastName, mobile);

        if (wage <= 0) {
            throw new IllegalArgumentException("Salary must be Greater then Zero");
        }
        if (hour <= 0) {
            throw new IllegalArgumentException("Hour must be Greater then Zero");
        }
        this.wage = wage;
        this.hour = hour;

    }

    public double getWage() {
        return wage;
    }

    public int getHour() {
        return hour;
    }


    @Override
    public double earnings() {
        double total = 0;
        if (hour <= 40) {
            total = calculateBasic();
        }
        if (hour > 40) {
            total = calculateBasic() + overTime();
        }
        return total;
    }

    public double calculateBasic() {
        return wage * hour;
    }

    public double overTime() {
        return (hour - 40) * (wage * 1.5);
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "Wage = " + wage + "\n" +
                "Hour = " + hour;
    }
}
