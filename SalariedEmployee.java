package abstractclasspolymorphysom;

public class SalariedEmployee extends Employee {
    private double wage;
    private byte week;

    public SalariedEmployee(String firstName, String lastName, String mobile, double wage, byte week) {
        super(firstName, lastName, mobile);

        if( wage <=0){
           throw new IllegalArgumentException("Salary must be Greater then Zero");
        }
        if( week <=0 || week >4){
            throw new IllegalArgumentException("Week between (1-4)");
        }
        this.wage = wage;
        this.week = week;
    }

    public double getWage() {
        return wage;
    }

    public int getWeek() {
        return week;
    }


    @Override
    public double earnings() {
        return wage * week;
    }

    @Override
    public String toString() {
        return super.toString()+"\n"+
                "Wage = " + wage +"\n"+
                "Week = " + week;
    }
}
