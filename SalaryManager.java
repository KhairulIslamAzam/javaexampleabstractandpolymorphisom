package abstractclasspolymorphysom;

import java.util.Arrays;

public class SalaryManager {
    private Employee[] employees = new Employee[4];

    public void calculateEarning() {
        for (Employee employee : employees) {
            System.out.println(printfInfo(employee));
            System.out.println("Total Earning: " + employee.earnings());
            System.out.println();
                
        }
    }

    public String printfInfo(Employee employee) {
        String fullInfo = null;

        if (employee instanceof CommisionEmployee) {
            fullInfo = "Commision Employee \n" + employee.toString();
        }

        if (employee instanceof BasePlusCommisionEmployee) {
            fullInfo = "BasePlusCommission Employee \n" + employee.toString();
        }

        if (employee instanceof SalariedEmployee) {
            fullInfo = "Salaried Employee \n" + employee.toString();
        }

        if (employee instanceof HourlyEmployee) {
            fullInfo = "Hourly Employee \n" + employee.toString();
        }

        return fullInfo;
    }

    {
        Employee employee = new CommisionEmployee("Khairul", "Islam", "123", .5, 20000);
        Employee employee1 = new BasePlusCommisionEmployee("Nazim", "Uddin", "234", .15, 10000, 500);
        //Employee employee4 = new BasePlusCommisionEmployee("Nazim", "Uddin", "234", 1, 10000, 500);
        Employee employee2 = new SalariedEmployee("Jobaer", "Uddin", "456", 1000, (byte) 1);
        Employee employee3 = new HourlyEmployee("Yousha", "Mahmud", "389", 1000, 45);

        employees[0] = employee;
        employees[1] = employee1;
        employees[2] = employee2;
        employees[3] = employee3;
        // employees[4] = employee4;

    }
}
